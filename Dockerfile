FROM python:3.9-slim-bullseye

ENV VIRTUAL_ENV=/opt/venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

# Install dependencies:
COPY requirements.txt .
RUN pip install -r requirements.txt

# Run the application:
COPY . .

FROM library/postgres
ENV POSTGRES_USER admin_ps
ENV POSTGRES_PASSWORD post1234
ENV POSTGRES_DB messenger_db
