from django.forms import widgets
from django.shortcuts import render
from django.http import HttpResponse
from pywebio.input import *
from .models import User, Message
from .forms import MessagesForm


def home(request):
    users = User.objects.all()
    return render(request, 'send_message/home.html')

# def index(request):
#     users = User.objects.all()
#     return render(request, 'send_message/index.html')

def create_message(request):
    error = ''
    if request.method == "POST":
        form = MessagesForm(request.POST)
        if form.is_valid():
            form.save()
        else: 
            error = "Косяк"

    form = MessagesForm()

    data = {
        'form': form,
        'error': error
    }
    users = User.objects.all()
    print(request)
    return render(request, 'send_message/create.html', data)

def test(request):
    return HttpResponse("TESTING")
# Create your views here.