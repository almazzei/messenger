from .models import User, Message
from django.forms import ModelForm, TextInput

class MessagesForm(ModelForm):

    def __init__(self, *args, **kwargs):
        
        super().__init__(*args, **kwargs)
        self.fields['user'].empty_label = 'Пользователь не выбран'
        self.fields['recipient'].empty_label = 'Адресат не выбран'

    class Meta:

        model = Message
        fields = ['message_text', 'recipient', 'user' ]
        widgets = {
            "message_text": TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Текст сообщения',
            }),
            # "user": TextInput(attrs={
            #     'class': 'form-control',
            #     'placeholder': 'Автор'
            # }),
            # "recipient": TextInput(attrs={
            #     'class': 'form-control',
            #     'placeholder': 'Адресат'
            # }),
    }

    