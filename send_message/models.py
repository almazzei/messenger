from django.db import models

# Create your models here.

class User(models.Model):

    email = models.CharField('e-mail пользователя', max_length=50)
    name = models.CharField('Имя пользователя',default='user', max_length=20)
    # password = models.CharField('пароль пользователя', max_length=20)

    def __str__(self):
        return self.name

    def get_absolute_url(self): 
        pass

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'

class Message(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=False)
    message_text = models.TextField('Текст сообщения')
    #вот тут продумай, нужно чтобы сообщения удалялись только если отправитель удалился
    recipient = models.ForeignKey(User,on_delete=models.SET_NULL, related_name='+', null=True)

    def __str__(self):
        return self.message_text

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'